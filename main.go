package main

import (

	// Import the generated protobuf code
	"fmt"

	micro "github.com/micro/go-micro"
	pb "gitlab.com/krufyliu/consignment-service/proto/consignment"
	vesselPb "gitlab.com/krufyliu/vessel-service/proto/vessel"
)

func main() {

	repo := &ConsignmentRepository{}

	// Create a new service. Optionally include some options here.
	srv := micro.NewService(

		// This name must match the package name given in your protobuf definition
		micro.Name("go.micro.srv.consignment"),
		micro.Version("latest"),
	)

	vesselClient := vesselPb.NewVesselService("go.micro.srv.vessel", srv.Client())

	// Init will parse the command line flags.
	srv.Init()

	// Register handler
	pb.RegisterShippingServiceHandler(srv.Server(), &service{repo, vesselClient})

	// Run the server
	if err := srv.Run(); err != nil {
		fmt.Println(err)
	}
}
